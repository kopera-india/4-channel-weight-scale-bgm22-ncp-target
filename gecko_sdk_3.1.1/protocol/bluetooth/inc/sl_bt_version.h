/***************************************************************************//**
 * @brief Bluetooth stack version definition
 *******************************************************************************
 * # License
 * <b>Copyright 2018 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * The licensor of this software is Silicon Laboratories Inc. Your use of this
 * software is governed by the terms of Silicon Labs Master Software License
 * Agreement (MSLA) available at
 * www.silabs.com/about-us/legal/master-software-license-agreement. This
 * software is distributed to you in Source Code format and is governed by the
 * sections of the MSLA applicable to Source Code.
 *
 ******************************************************************************/

#ifndef SL_BT_VERSION_H
#define SL_BT_VERSION_H

#define BG_VERSION_MAJOR 3
#define BG_VERSION_MINOR 1
#define BG_VERSION_PATCH 2
#define BG_VERSION_BUILD 256
#define BG_VERSION_HASH  {0xe8,0x46,0x35,0x25,0xfd,0xd4,0xee,0x78,0x67,0x53,0xe5,0x81,0x8a,0x1a,0xa9,0x8c,0xf4,0x16,0xa6,0xe8}

#endif
